import pickle
import aiohttp
from aiohttp import web
import redis
from config import FILE_STORAGE_TIME


class Message(object):
    def __init__(self):
        self.redis = redis.StrictRedis(host='localhost', port=6379, db=3)

    # It needs to be rewrite
    def save(self, msg, key):
        if key and msg:
            data = pickle.loads(self.redis.get(key))
            data['messages'].append(msg)
            return self.redis.set(key, pickle.dumps(data), ex=86400 * FILE_STORAGE_TIME)
        return None


    def get_messages(self, key):
        data = []
        if self.redis.exists(key):
            data = pickle.loads(self.redis.get(key))['messages']
        return data


async def websocket_handler(request):
    key = request.query['key']
    ws = web.WebSocketResponse()
    message = Message()

    await ws.prepare(request)
    await ws.send_str('You connected to the chat')

    for _msg in message.get_messages(key):
        await ws.send_str(_msg)

    if not request.app['websockets']:
        request.app['websockets'] = {}
    if not key in request.app['websockets']:
        request.app['websockets'][key] = []

    request.app['websockets'][key].append(ws)

    for _ws in request.app['websockets'][key]:
        if _ws is not ws:
            await _ws.send_str('User joined')

    async for msg in ws:
        if msg.type == aiohttp.WSMsgType.TEXT:
            if len(msg.data) > 40:
                await ws.send_str('message too long')
            else:
                message.save(msg.data, key)
                for _ws in request.app['websockets'][key]:
                    if _ws is not ws:
                        await _ws.send_str(msg.data)

        elif msg.type == aiohttp.WSMsgType.ERROR:
            print('ws connection closed with exception %s' %
                  ws.exception())

    request.app['websockets'][key].remove(ws)

    print('websocket connection closed')
    return ws
