import logging
from aiohttp import web
from views import websocket_handler


async def init_app():
    app = web.Application()
    app['websockets'] = {}
    app.on_shutdown.append(shutdown)
    app.router.add_get('/ws_chat', websocket_handler)
    return app


async def shutdown(app):
    for ws_list in app['websockets'].values():
        for ws in ws_list:
            await ws.close()
    app['websockets'].clear()


def main():
    logging.basicConfig(level=logging.DEBUG)
    app = init_app()
    web.run_app(app, port=8010)


if __name__ == '__main__':
    main()